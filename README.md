__README.md__ 

* clone the repository:                                                                                                                                                   > git clone https://mark_rudolph@github.com/josefK128/backpropagation.git
  
* install python 3.X (current latest 3.8) - see 
  https://www.python.org/downloads/ - simply click download button and follow 
  defaults for installation
  
* install numpy:  > pip install numpy
* install sys:  > pip install sys

* dni.py usage:  > py dni.py (>log)
  This command will write out all diagnostics to the console (or to a logfile)
  

